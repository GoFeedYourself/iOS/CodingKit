//
//  CodingKeys.swift
//  CodingKit
//
//  Created by Maxim Krouk on 4/11/20.
//

public struct CodingKeys: CodingKey {
    
    public var stringValue: String
    
    public init(stringValue: String) {
        self.stringValue = stringValue
    }
        
    @available(*, deprecated, message: "Always fails.")
    public init?(intValue: Int) { nil }
    public var intValue: Int? { .none }
    
    public static var type: Self { .init(stringValue: "Type") }
    public static func custom(_ value: String) -> Self { .init(stringValue: value) }
    
}

extension CodingKeys: ExpressibleByStringLiteral {
    
    public init(stringLiteral value: String) {
        self = .custom(value)
    }
    
}
