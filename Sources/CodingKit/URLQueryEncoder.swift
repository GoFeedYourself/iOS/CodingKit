//
//  URLQueryEncoder.swift
//  CodingKit
//
//  Created by Maxim Krouk on 2/19/20.
//

import Foundation
import Combine

@available(OSX 10.15, iOS 13.0, tvOS 13.0, watchOS 6.0, *)
extension URLQueryEncoder: TopLevelEncoder {}

public class URLQueryEncoder {
    
    public typealias Output = Data
    
    public func encode<T>(_ value: T) throws -> [URLQueryItem] where T : Encodable {
        let encoder = _URLQueryEncoder()
        try value.encode(to: encoder)
        return encoder.encodingContainer.data.queryItems ?? []
    }
    
    public func encode<T>(_ value: T) throws -> Data where T : Encodable {
        let encoder = _URLQueryEncoder()
        try value.encode(to: encoder)
        let query = encoder.encodingContainer.data.percentEncodedQuery ?? ""
        return query.data(using: .utf8) ?? .init()
    }
    
}

private class _URLQueryEncoder: Encoder {
    
    public var codingPath: [CodingKey] = []
    
    public var userInfo: [CodingUserInfoKey:Any] = [:]
    
    private(set) internal var encodingContainer: _EncodingContainer
    
    public init() {
        self.encodingContainer = _EncodingContainer()
    }
    
    fileprivate init(container: _EncodingContainer) {
        self.encodingContainer = container
    }
    
    public func container<Key>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key> where Key : CodingKey {
        var container = _KeyedContainer<Key>(wrapping: encodingContainer)
        container.codingPath = codingPath
        return KeyedEncodingContainer(container)
    }
    
    public func unkeyedContainer() -> UnkeyedEncodingContainer {
        var container = _UnkeyedContainer(wrapping: encodingContainer)
        container.codingPath = codingPath
        return container
    }
    
    public func singleValueContainer() -> SingleValueEncodingContainer {
        var container = _SingleValueContainer(wrapping: encodingContainer)
        container.codingPath = codingPath
        return container
    }
    
}

private extension _URLQueryEncoder {
    class _EncodingContainer {
        private(set) var data: URLComponents
        
        init() {
            data = .init()
            data.queryItems = []
        }
        
        func encode(key codingKey: [CodingKey], value: String?) {
            let key = codingKey.map { $0.stringValue }.joined(separator: ".")
            let queryItem = URLQueryItem(name: key, value: value)
            data.queryItems?.append(queryItem)
        }
    }
    
    struct _KeyedContainer<Key: CodingKey>: KeyedEncodingContainerProtocol {
        
        private var container: _EncodingContainer
        
        init(wrapping container: _EncodingContainer = .init()) {
            self.container = container
        }
        
        var codingPath: [CodingKey] = []
        
        mutating func encodeNil(forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: nil)
        }
        
        mutating func encode(_ value: Bool, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: String, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Double, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Float, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Int, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Int8, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Int16, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Int32, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: Int64, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: UInt, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: UInt8, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: UInt16, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: UInt32, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode(_ value: UInt64, forKey key: Key) throws {
            container.encode(key: codingPath + [key], value: value.description)
        }
        
        mutating func encode<T>(_ value: T, forKey key: Key) throws where T : Encodable {
            let encoder = _URLQueryEncoder(container: container)
            encoder.codingPath.append(key)
            try value.encode(to: encoder)
        }
        
        mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: Key)
        -> KeyedEncodingContainer<NestedKey> where NestedKey : CodingKey {
            var container = _KeyedContainer<NestedKey>(wrapping: self.container)
            container.codingPath.append(key)
            return .init(container)
        }
        
        mutating func nestedUnkeyedContainer(forKey key: Key) -> UnkeyedEncodingContainer {
            var container = _UnkeyedContainer(wrapping: self.container)
            container.codingPath.append(key)
            return container
        }
        
        mutating func superEncoder() -> Encoder {
            let superKey = Key(stringValue: "super")!
            return superEncoder(forKey: superKey)
        }
        
        mutating func superEncoder(forKey key: Key) -> Encoder {
            let encoder = _URLQueryEncoder(container: container)
            encoder.codingPath.append(key)
            return encoder
        }
        
    }
    
    struct _UnkeyedContainer: UnkeyedEncodingContainer {
        private let container: _EncodingContainer

        init(wrapping container: _EncodingContainer) {
            self.container = container
        }

        var codingPath: [CodingKey] = []

        private(set) var count: Int = 0

        private mutating func nextIndexedKey() -> CodingKey {
            let nextCodingKey = IndexedCodingKey(intValue: count)!
            count += 1
            return nextCodingKey
        }

        private struct IndexedCodingKey: CodingKey {
            let intValue: Int?
            let stringValue: String

            init?(intValue: Int) {
                self.intValue = intValue
                self.stringValue = intValue.description
            }

            init?(stringValue: String) { nil }
        }

        mutating func encodeNil() throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: nil)
        }

        mutating func encode(_ value: Bool) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: String) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value)
        }

        mutating func encode(_ value: Double) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Float) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Int) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Int8) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Int16) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Int32) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: Int64) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: UInt) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: UInt8) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: UInt16) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: UInt32) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode(_ value: UInt64) throws {
            container.encode(key: codingPath + [nextIndexedKey()], value: value.description)
        }

        mutating func encode<T: Encodable>(_ value: T) throws {
            let encoder = _URLQueryEncoder(container: container)
            encoder.codingPath.append(nextIndexedKey())
            try value.encode(to: encoder)
        }

        mutating func nestedContainer<NestedKey: CodingKey>(keyedBy keyType: NestedKey.Type)
        -> KeyedEncodingContainer<NestedKey> {
            var container = _KeyedContainer<NestedKey>(wrapping: self.container)
            container.codingPath.append(nextIndexedKey())
            return KeyedEncodingContainer(container)
        }

        mutating func nestedUnkeyedContainer() -> UnkeyedEncodingContainer {
            var container = _UnkeyedContainer(wrapping: self.container)
            container.codingPath.append(nextIndexedKey())
            return container
        }

        mutating func superEncoder() -> Encoder {
            let encoder = _URLQueryEncoder(container: container)
            encoder.codingPath.append(nextIndexedKey())
            return encoder
        }
    }
    
    struct _SingleValueContainer: SingleValueEncodingContainer {

        private let container: _EncodingContainer

        init(wrapping container: _EncodingContainer) {
            self.container = container
        }

        var codingPath: [CodingKey] = []

        mutating func encodeNil() throws {
            container.encode(key: codingPath, value: nil)
        }

        mutating func encode(_ value: Bool) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: String) throws {
            container.encode(key: codingPath, value: value)
        }

        mutating func encode(_ value: Double) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Float) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Int) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Int8) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Int16) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Int32) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: Int64) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: UInt) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: UInt8) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: UInt16) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: UInt32) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode(_ value: UInt64) throws {
            container.encode(key: codingPath, value: value.description)
        }

        mutating func encode<T: Encodable>(_ value: T) throws {
            let encoder = _URLQueryEncoder(container: container)
            encoder.codingPath = codingPath
            try value.encode(to: encoder)
        }
    }
}
