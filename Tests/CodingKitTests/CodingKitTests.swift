import XCTest
@testable import CodingKit

final class CodingKitTests: XCTestCase {
    
    func testNestedValuesDecoding() {
        struct Contact: Codable, Equatable {
            var id: Int
            @NestedCodable<FirstNameKeys, String>
            var firstName: String
            @NestedCodable<LastNameKeys, String?>
            var lastName: String?
            @NestedCodable<AddressKeys, String>
            var address: String
            
            init(id: Int, firstName: String, lastName: String?, address: String) {
                self.id = id
                self.firstName = firstName
                self.lastName = lastName
                self.address = address
            }
            
            // here just for compiler, does not decode anything
            private var user: CodingProxy = .init()
            
            enum FirstNameKeys: String, CodingKey, CaseIterable {
                case user, details, name, first
            }
            enum LastNameKeys: String, CodingKey, CaseIterable {
                case user, details, name, last
            }
            enum AddressKeys: String, CodingKey, CaseIterable {
                case user, details, address
            }
        }
        
        let json = """
        {
          "id" : 1,
          "user" : {
            "details" : {
              "name" : {
                "first" : "Swift",
                "last" : "Language"
              },
              "address" : "Apple St."
            }
          }
        }
        """
        let data = json.data(using: .utf8)!
        do {
            let person = try JSONDecoder().decode(Contact.self, from: data)
            XCTAssertEqual(person, Contact(id: 1,
                                           firstName: "Swift",
                                           lastName: "Language",
                                           address: "Apple St."))
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let newData = try encoder.encode(person)
            XCTAssertEqual(newData, data)
        } catch {
            XCTFail("Something went wrong :(")
        }

    }
    
    func testURLQueryEndcoder() {
        struct TokenRequest: Codable {
            var grantType: String?
            var username: String = "testUN"
            var password: String = "testPW"
            var scope: String?
            var clientId: String?
            var clientSecret: String?
        }
        
        let encoder = URLQueryEncoder()
        print(String(data: try! encoder.encode(TokenRequest()), encoding: .utf8)!)
        XCTAssertEqual([URLQueryItem(name: "username", value: "testUN"),
                        URLQueryItem(name: "password", value: "testPW")],
                       try! encoder.encode(TokenRequest()))
    }

    static var allTests = [
        ("testNestedValuesDecoding", testNestedValuesDecoding),
        ("testURLQueryEndcoder", testURLQueryEndcoder)
    ]
}
