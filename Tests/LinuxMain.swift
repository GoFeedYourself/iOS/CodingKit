import XCTest

import CodingKitTests

var tests = [XCTestCaseEntry]()
tests += CodingKitTests.allTests()
XCTMain(tests)
